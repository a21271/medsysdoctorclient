package com.sixplus.med_sys_doctor;

import com.sixplus.med_sys_doctor.module.MedSysData;

import us.xdroid.util.xHelper;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;
import android.widget.Toast;

public class PatientListActivity extends Activity 
{

	private PatientListFragment localPatientListFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        localPatientListFragment = (PatientListFragment)getFragmentManager().findFragmentById(R.id.patient_list_fragment);
        setOnContentView();

        //title
        ((TextView)findViewById(R.id.navigation_title)).setText(R.string.patient_list);
    }
    
    protected void setOnContentView()
    {
    	xHelper.log("setOnContentView");
    	this.setContentView(R.layout.patient_list);
    }
    

}
