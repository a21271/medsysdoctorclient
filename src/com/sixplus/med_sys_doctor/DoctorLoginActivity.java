package com.sixplus.med_sys_doctor;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import 	android.content.Intent;

import com.sixplus.med_sys_doctor.MedController;

public class DoctorLoginActivity extends Activity {
	private static final String TAG = "DoctorLoginActivity";
    // Progress Dialog
    private ProgressDialog pDialog;
    //private TextView tv_head;
    EditText mUsernameText;
    EditText mPasswordText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_view);
        
        //Must init the MedController here!!!
  	  	if(MedController.mController == null) {
  	  		MedController controller = new MedController(this);
  	  	}

  	  	//Set the UI handler
  	  	MedController.setUIHandler(mUiHandler);
  	  
        // Edit Text
        mUsernameText = (EditText) findViewById(R.id.username);
        mPasswordText = (EditText) findViewById(R.id.password);
        TextView mLogin = (TextView)findViewById(R.id.login);
        mLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            	String username = mUsernameText.getText().toString().trim();
            	String password = mPasswordText.getText().toString().trim();

            	if(!validate(username, password))
            		return;

            	//do the login in background
            	MedController.login(username, password);

            	//show progress dialog
            	pDialog = new ProgressDialog(DoctorLoginActivity.this);
                pDialog.setMessage("Process...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
            }
        });
    }

    private boolean validate(String username, String password)
    {
       String enptyNameOrPassword = getResources().getString(R.string.emptynameorpassword);
        if (username.equals("") || password.equals(""))
        {
            DialogUtil.showDialog(this, enptyNameOrPassword, false);
            return false;
        }
        return true;
    }

    //implement the UI handler
    final Handler mUiHandler = new Handler(new Handler.Callback() {
      @Override
      public boolean handleMessage(Message msg) {
          switch (msg.what) {
				case  MedController.LOGIN:
                    //Medcontroller will sent this message back after it handled the request
					Log.d(TAG, "msg.arg1 = " + msg.arg1);
					if(msg.arg1 == 0) {
						Toast.makeText(DoctorLoginActivity.this, "login success", Toast.LENGTH_LONG).show();
						//remove it to login success
						Intent intent = new Intent(Intent.ACTION_VIEW);
						intent.setClass(DoctorLoginActivity.this, DoctorDashboardActivity.class);
						startActivity(intent);
						
						finish();
					}
					else {
						Toast.makeText(DoctorLoginActivity.this,getString(R.string.username_error), Toast.LENGTH_LONG).show();
					}
					pDialog.dismiss();
                  break;
		  }
          
          return false;
	   }
	});
}
