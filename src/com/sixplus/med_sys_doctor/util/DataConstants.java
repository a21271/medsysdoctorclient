package com.sixplus.med_sys_doctor.util;

public interface DataConstants {
	final int ATTACH_TYPE_NONE = 0;
	final int ATTACH_TYPE_WAV = 1;
	final int ATTACH_TYPE_PIC = 2;
	
	final String TAG_PATIENT_TYPE = "patient_type";
	final String PATIENT_TYPE_ASKING_QUESTION = "asking_question";
	final String PATIENT_TYPE_QUESTION_READ = "question_read";
	final String PATIENT_TYPE_QUESTION_OLD = "quesiont_old";
}
	