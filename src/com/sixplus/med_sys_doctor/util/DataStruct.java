package com.sixplus.med_sys_doctor.util;

import static com.sixplus.med_sys_doctor.util.DataConstants.*;

public class DataStruct {
	
	public static class Patient {
		/*
         *  `pat_id` BIGINT NOT NULL ,
            `login_id` VARCHAR(45) NOT NULL ,
            `login_password` VARCHAR(45) NOT NULL ,
            `role_type` INT NOT NULL ,
            `name` VARCHAR(45) NOT NULL ,
            `sex` TINYINT NOT NULL ,
            `address` VARCHAR(100) NULL ,
            `birthdate` DATE NULL ,
         */
		public Patient() {
		}
		public long pat_id;               
		public String login_id;		
		public String name;
		public int sex;
		public String address;
		public String birthdate;
	}
	
	public static class PatientQuestion {
	    /*
        `seq_id` bigint(20) NOT NULL auto_increment,
        `pat_id` bigint(20) default NULL,
        `doc_id` bigint(20) default NULL,
        `question_id` int(11) NOT NULL COMMENT 'a question_id may contains several message_ids',
        `message_id` int(11) NOT NULL COMMENT 'every message has a message_id',
        `direct` tinyint(4) NOT NULL,
        `question_desc` varchar(100) default NULL,
        `attach_type` tinyint(4) NOT NULL,
        `attach_url` varchar(200) default NULL,
        `create_date` date NOT NULL,
        `read_date` date default NULL,

		 */
		public PatientQuestion(PatientQuestion p) {
			seq_id = p.seq_id;
			doc_id = p.doc_id;
			pat_id = p.pat_id;
			question_id = p.question_id;
			message_id = p.message_id;
			direct = p.direct;
			question_desc = p.question_desc;
			attach_type = p.attach_type;
			attach_url = p.attach_url;
			attach_local_url = p.attach_local_url;
		}
		
		public PatientQuestion() {
			
		}
		
		public long seq_id;
		public long doc_id;
		public long pat_id;
		public int question_id;
		public int message_id;
		public int direct;
		public String question_desc;
		public int attach_type;
		public String attach_url;
		public String attach_local_url;
		public String create_date;
		public String read_date;
	}
	
	public static class Doctor {
	    /*

        `doc_id` bigint(20) NOT NULL,
        `login_id` varchar(45) NOT NULL,
        `login_password` varchar(45) NOT NULL,
        `name` varchar(45) NOT NULL,
        `sex` tinyint(4) NOT NULL,
        `role_type` tinyint(4) NOT NULL,
        `address` varchar(100) default NULL,
        `auth_code` bigint(20) NOT NULL,
        `picture` mediumblob,
        `pos_id` tinyint(4) NOT NULL,
        `dep_id` tinyint(4) NOT NULL,
        `update_date` date NOT NULL,
        `description` varchar(500) default NULL,
		 */
		public Doctor() {
		}
		public long doc_id;
		public long login_id;
		public String name;
		public int sex;
		public int role_type;
		public long auth_code;
	}
	
	public static class QuestionDigest {
		public QuestionDigest() {
			
		}
		
		public long question_id;
		public String create_date;
		public long local_max_seq_id;
		public long remote_max_seq_id;
		public boolean new_msg;
	}
}