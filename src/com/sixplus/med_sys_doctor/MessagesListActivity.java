package com.sixplus.med_sys_doctor;

import us.xdroid.util.xHelper;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class MessagesListActivity extends Activity 
{

	//private PatientQuestionListFragment localPatientListFragment;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      
        setOnContentView();

        //title
        ((TextView)findViewById(R.id.navigation_title)).setText(R.string.patient_asked_question);
        
        final MessagesListFragment quesFragment = (MessagesListFragment)getFragmentManager().findFragmentById(R.id.patient_question_list_fragment);
        // submit button
        
        final ImageButton submitBtn = (ImageButton) findViewById(R.id.submit);
        final EditText txDiagnose = (EditText) findViewById(R.id.text_diagnose);
        submitBtn.setOnClickListener(new View.OnClickListener() {
    		
    		@Override
    		public void onClick(View arg0) {
    			// TODO Auto-generated method stub
    			quesFragment.addDocResponse(txDiagnose.getText().toString());
    			txDiagnose.setText("");
    			quesFragment.refreshUI();
    		}
    	});

    }
    
    protected void setOnContentView()
    {
    	xHelper.log("setOnContentView");
    	this.setContentView(R.layout.message_list);
    }
    

    
}
