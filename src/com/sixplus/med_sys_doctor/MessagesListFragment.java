package com.sixplus.med_sys_doctor;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import com.haarman.listviewanimations.swinginadapters.prepared.SwingLeftInAnimationAdapter;
import com.sixplus.med_sys_doctor.module.MedSysData;
import com.sixplus.med_sys_doctor.util.DataConstants;
import com.sixplus.med_sys_doctor.util.DataStruct.PatientQuestion;

import us.xdroid.util.xHelper;


public class MessagesListFragment extends Fragment
  implements AdapterView.OnItemClickListener
{
  private ListView mListView;
  private ProgressDialog mProgressDialog;
  QuestionListAdapter mListAdapter;

  @Override
public void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	
	//Set the UI handler
	MedController.setUIHandler(mUiHandler);
	
	//Get the patient's questions
	Intent intent = getActivity().getIntent();
	long questID = intent.getLongExtra("question_id", -1);
	MedController.getMessagesOfQuestion(questID);
	
	//Show the process dialog
	showProgressDialog();
}
  	public void onActivityCreated(Bundle paramBundle)
  	{
  		super.onActivityCreated(paramBundle);
    
  	}
  	
  	public void refreshUI()
  	{
  		mListAdapter.notifyDataSetChanged();
  		if(mListAdapter.getCount() > 0) {
  			mListView.setSelection(mListAdapter.getCount() - 1);
  		}
  	}
  	
  	public void addDocResponse(String resp)
  	{
  		//add the data to local
  		MedSysData.addDocResponse(resp);
  		
  		//add the data to server
  		MedController.uploadDocResponse(MedSysData.mMessageList.get(MedSysData.mMessageList.size() - 1));
  	}
  	
	private void showProgressDialog()
	{
    	mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Process...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();
    }
	
	private void closeProgressDialog() {
    	mProgressDialog.dismiss();
    }
    
    private void onGetPatientQuestionListDone(int arg) {
  	  closeProgressDialog();
  	  refreshUI();
    }
    
    final Handler mUiHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case MedController.REQ_MSGS_OF_QUESTION:
                	if(msg.arg1 == 0) {
  						Toast.makeText(getActivity(), "req questions success", Toast.LENGTH_LONG).show();
  						
  						//Download the picture from server
  						for(PatientQuestion q : MedSysData.mMessageList) {
  							if( (q.attach_type == DataConstants.ATTACH_TYPE_WAV || q.attach_type == DataConstants.ATTACH_TYPE_PIC)
  									&& q.attach_local_url == null 
  									&& !(q.attach_url == null)
  									&& !q.attach_url.equals("null")
  									&& !q.attach_url.equals("NULL")) {
  								MedController.downloadAttachment(q.attach_url, q.pat_id, q.question_id, q.message_id, q.attach_type);
  								break;
  							}
  						}
  					}
  					else {

  						Toast.makeText(getActivity(), "req questions failed!", Toast.LENGTH_LONG).show();
  					}
                	
                	onGetPatientQuestionListDone(msg.arg1);
                	
  					break;

  				case MedController.DL_ATTACHMENT:
  					if(msg.arg1 == 0) {
  						Toast.makeText(getActivity(), "download attachment success", Toast.LENGTH_LONG).show();
  						mListAdapter.notifyDataSetChanged();

  						//Download left attachment from server
  						boolean noDl = true;
  						for(PatientQuestion q : MedSysData.mMessageList) {
  							if( (q.attach_type == DataConstants.ATTACH_TYPE_WAV || q.attach_type == DataConstants.ATTACH_TYPE_PIC)
  									&& q.attach_local_url == null 
  									&& !(q.attach_url == null)
  									&& !q.attach_url.equals("null")
  									&& !q.attach_url.equals("NULL")) {
  								noDl = false;
  								MedController.downloadAttachment(q.attach_url, q.pat_id, q.question_id, q.message_id, q.attach_type);
  								break;
  							}
  						}
  					}
  					else {
  						Toast.makeText(getActivity(), "download attachment failed!", Toast.LENGTH_LONG).show();
  					}
  					onGetPatientQuestionListDone(msg.arg1);
  					break;		
  		  } // end of switch
           return false;
  	   }
  	});

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    
    mListView= new ListView(getActivity());
    mListView.setOnItemClickListener(this);
    
    mListAdapter = new QuestionListAdapter(getActivity(), MedSysData.mMessageList);
    SwingLeftInAnimationAdapter swingLeftInAnimationAdapter = new SwingLeftInAnimationAdapter(mListAdapter);
    swingLeftInAnimationAdapter.setAbsListView(mListView);
    mListView.setAdapter(swingLeftInAnimationAdapter);

    mListView.setCacheColorHint(0);

    return mListView;
  }

  void doRetrieve(){
	  
  }
  
  private View.OnClickListener imgPreviewClickListener = new View.OnClickListener() {

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		String fileName = (String) v.getTag();
		Intent intent = new Intent();
		intent.setClass(getActivity(), PatientViewPhotoActivity.class);
		intent.putExtra("imagePath", fileName);
		startActivity(intent);

	}
	  
  };
  
  private View.OnClickListener wavIconClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			//Toast.makeText(getActivity(), "Wave icon pressed", Toast.LENGTH_SHORT).show();
			String fileName = (String) v.getTag();
			xHelper.log(fileName);
			
			Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse(fileName), "video/*");
            startActivity(intent);

		}

	  };


  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
	  
	  /*Intent intent = new Intent(Intent.ACTION_VIEW);
	  intent.setClass(this.getActivity(),  PatientAskedQuestionActivity.class);
	  intent.putExtra("patient_index", paramInt-1);
	  startActivity(intent);
      */
  }


  public void onPause()
  {
    super.onPause();
  }

  public void onResume()
  {
    super.onResume();
  }

  class QuestionListAdapter extends com.haarman.listviewanimations.ArrayAdapter<PatientQuestion>{
      Context mContext;
      ArrayList<PatientQuestion> mItems;
      
      public QuestionListAdapter(Context c, ArrayList<PatientQuestion> items) {
    		super(items);
    		mItems = items;
    		mContext = c;
      }
      
      @Override
      public int getCount() {
    	  return mItems.size();
      }
      
      @Override
      public long getItemId(int position) {
          // TODO Auto-generated method stub
          return position;
      }
      
  	  @Override
  	  public PatientQuestion getItem(int arg0) {
  		  // TODO Auto-generated method stub
  		  //xHelper.log("get " + arg0 + " : " + mPatQuestionList.get(arg0).toString());
  		  return mItems.get(arg0);
  	  }
      
      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
          LayoutInflater inflater = LayoutInflater.from(mContext);
          View localView = inflater.inflate(R.layout.message_item, null);

		  PatientQuestion patQues = getItem(position);
		  
		  TextView tvLeftMsg = (TextView) localView.findViewById(R.id.left_message);
		  TextView tvRightMsg = (TextView) localView.findViewById(R.id.right_message);
		  ImageView tvLeftIcon = (ImageView) localView.findViewById(R.id.left_icon);
		  ImageView tvRightIcon = (ImageView) localView.findViewById(R.id.right_icon);
		  ImageView tvLeftImgPreview = (ImageView) localView.findViewById(R.id.left_img_preview);
		  ImageView tvLeftWavIcon = (ImageView) localView.findViewById(R.id.left_wav_icon);
		  
		  if(patQues.direct == 0) {
			switch(patQues.attach_type) {
			case 0: // no attach
				tvLeftImgPreview.setVisibility(View.GONE);
				tvLeftWavIcon.setVisibility(View.GONE);
				tvLeftMsg.setVisibility(View.VISIBLE);
				tvLeftMsg.setText(patQues.question_desc);
				
				tvRightMsg.setVisibility(View.GONE);
				tvRightIcon.setVisibility(View.GONE);
				break;
			case 1: // wave
				tvLeftImgPreview.setVisibility(View.GONE);
				tvLeftWavIcon.setVisibility(View.VISIBLE);
				tvLeftMsg.setVisibility(View.GONE);
				
				tvRightMsg.setVisibility(View.GONE);
				tvRightIcon.setVisibility(View.GONE);
				
				//set the tag
				if(patQues.attach_local_url != null) {
					tvLeftWavIcon.setOnClickListener(wavIconClickListener);
					tvLeftWavIcon.setTag(patQues.attach_local_url);
				}
				break;
			case 2: // picture
				tvLeftImgPreview.setVisibility(View.VISIBLE);
				tvLeftWavIcon.setVisibility(View.GONE);
				tvLeftMsg.setVisibility(View.GONE);

				// show picture
				if(patQues.attach_local_url != null) {
					tvLeftImgPreview.setOnClickListener(imgPreviewClickListener);
					tvLeftImgPreview.setTag(patQues.attach_local_url);

					Bitmap bm = BitmapFactory.decodeFile(patQues.attach_local_url);
					
					tvLeftImgPreview.setImageBitmap(bm);
				}
				
				tvRightMsg.setVisibility(View.GONE);
				tvRightIcon.setVisibility(View.GONE);
				break;
			default:
			};
		  } else {
			    tvRightMsg.setText(patQues.question_desc);
			    
			    tvLeftMsg.setVisibility(View.GONE);
				tvLeftIcon.setVisibility(View.GONE);
				tvLeftImgPreview.setVisibility(View.GONE);
				tvLeftWavIcon.setVisibility(View.GONE);
		  }
			
          //xHelper.log("getView-------" + position);
          return localView;
      }


  }
  
  
}