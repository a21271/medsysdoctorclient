package com.sixplus.med_sys_doctor;

import java.util.ArrayList;

import com.haarman.listviewanimations.swinginadapters.prepared.SwingBottomInAnimationAdapter;
import com.sixplus.med_sys_doctor.PatientListFragment.PatientListAdapter;
import com.sixplus.med_sys_doctor.module.MedSysData;
import com.sixplus.med_sys_doctor.util.DataConstants;
import com.sixplus.med_sys_doctor.util.DataStruct.QuestionDigest;

import us.xdroid.util.xHelper;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.TextView;

public class PatientQuestionListActivity extends Activity 
implements AdapterView.OnItemClickListener
{
	private ListView mListView;
	private QuestionDigestListAdapter questDigestListAdapter;
	private ProgressDialog mProgressDialog;
	String mPatientType = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      
        setOnContentView();
       
        //title
        ((TextView)findViewById(R.id.navigation_title)).setText(R.string.question_digest);
        
        // quit button
        final Button quitBtn = (Button) findViewById(R.id.quit);
        quitBtn.setOnClickListener(new View.OnClickListener() {
    		
    		@Override
    		public void onClick(View arg0) {
    			finish();
    		}
    	});
        
        // get the patient type
        Intent intent = getIntent();
        mPatientType = intent.getStringExtra(DataConstants.TAG_PATIENT_TYPE);
             
        // set the listview
        mListView = (ListView) this.findViewById(R.id.id_list);
        mListView.setOnItemClickListener(this);
        if (mPatientType.equals(DataConstants.PATIENT_TYPE_ASKING_QUESTION)) {
        	questDigestListAdapter = new QuestionDigestListAdapter(this, MedSysData.mQuestionDigestList);
        	long patID = intent.getLongExtra("pat_id", -1);
        	
        	//get the old question id list from local
        	MedController.getOldQuestionsOfPatient(patID, MedSysData.getSelfID());
        	
        	//get the new question id list from server
        	MedController.getNewQuestionsOfPatient(patID, MedSysData.getSelfID());
        	
        	//Show the process dialog
        	showProgressDialog();
        } else if (mPatientType.equals(DataConstants.PATIENT_TYPE_QUESTION_READ)) {
        	//questDigestListAdapter = new QuestionDigestListAdapter(this, MedSysData.mReadQuestionList);
        } else if (mPatientType.equals(DataConstants.PATIENT_TYPE_QUESTION_OLD)) {
        	//questDigestListAdapter = new QuestionDigestListAdapter(this, MedSysData.mOldQuestionList);
        }
        SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(questDigestListAdapter);
        swingBottomInAnimationAdapter.setAbsListView(mListView);
        mListView.setAdapter(swingBottomInAnimationAdapter);
    	
    	//Set the UI handler
  	  	MedController.setUIHandler(mUiHandler);

    }
    
    //implement the UI handler
   final Handler mUiHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			// TODO Auto-generated method stub
			switch (msg.what) {
			case  MedController.REQ_PATIENT_QUESTIONS_LIST:
                //Medcontroller will sent this message back after it handled the request
				if(msg.arg1 == 0) {
					Toast.makeText(PatientQuestionListActivity.this, "get question id list success", Toast.LENGTH_LONG).show();
				}
				else {
					Toast.makeText(PatientQuestionListActivity.this, "get question id list failed!", Toast.LENGTH_LONG).show();
				}
				closeProgressDialog();
				questDigestListAdapter.notifyDataSetChanged();
              break;
	        }
			return false;
		}
	});
    
    protected void setOnContentView()
    {
    	xHelper.log("setOnContentView");
    	this.setContentView(R.layout.patient_question_id_list);
    }

	@Override
	public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(Intent.ACTION_VIEW);
		QuestionDigest tag = (QuestionDigest)paramView.getTag();
		
		intent.setClass(PatientQuestionListActivity.this,  MessagesListActivity.class);

		intent.putExtra("question_id", tag.question_id);
		intent.putExtra("max_seq_id", tag.local_max_seq_id);
		
		// start the activity
		startActivity(intent);
	}
	
	private void showProgressDialog() {
    	mProgressDialog = new ProgressDialog(PatientQuestionListActivity.this);
        mProgressDialog.setMessage("Process...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();
    }
	
	private void closeProgressDialog() {
    	mProgressDialog.dismiss();
    }
    
	  class QuestionDigestListAdapter extends com.haarman.listviewanimations.ArrayAdapter<QuestionDigest>{
	      Context mContext;
	      ArrayList<QuestionDigest> mItems;
	      
	      public QuestionDigestListAdapter(Context c, ArrayList<QuestionDigest> items) {
	  		super(items);
	  		mContext = c;
	  		mItems = items;
	  	  }
	      
	      @Override
	      public int getCount() {
	    	  //xHelper.log("MyListAdapter::getCount");
	    	  return mItems.size();
	      }
	      
	      @Override
	      public long getItemId(int position) {
	          // TODO Auto-generated method stub
	          return position;
	      }
	      
	      @Override
	      public View getView(int position, View convertView, ViewGroup parent) {
	          LayoutInflater inflater = LayoutInflater.from(mContext);
	          View localView = inflater.inflate(R.layout.patient_question_id_item, null);
	          QuestionDigest questDigest = getItem(position);
	         
	         ((TextView)localView.findViewById(R.id.quest_id)).setText(Long.toString(questDigest.question_id));
	         
	         // set tag for later use
	         localView.setTag(questDigest);

	          return localView;
	      }

		@Override
		public QuestionDigest getItem(int arg0) {
			// TODO Auto-generated method stub
			return mItems.get(arg0);
		}


	  }
    
}
