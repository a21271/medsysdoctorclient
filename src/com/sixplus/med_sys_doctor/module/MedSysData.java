package com.sixplus.med_sys_doctor.module;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import us.xdroid.util.xHelper;

import android.os.Environment;

import com.sixplus.med_sys_doctor.util.DataConstants;
import com.sixplus.med_sys_doctor.util.DataStruct.*;

public class MedSysData {
    
	// the new question list (unread & no-respond)
	public static ArrayList<QuestionDigest> mQuestionDigestList = new ArrayList<QuestionDigest>();
	
	// the read question list (read but no-respond)
	public static ArrayList<Long> mReadQuestionList = new ArrayList<Long>();
	
	// the old question list (read & responded)
	public static ArrayList<Long> mOldQuestionList = new ArrayList<Long>();
	
	// the patient list who are asking the questions
	public static ArrayList<Patient> mAskingQuesPatientList = new ArrayList<Patient>();
	
	// the patient list whose question has been read but no-respond
	public static ArrayList<Patient> mReadQuesPatientList = new ArrayList<Patient>();
	
	// the patient list whose question has been read and respond
	public static ArrayList<Patient> mOldQuesPatientList = new ArrayList<Patient>();
	
	// the current message list of one question
	public static ArrayList<PatientQuestion> mMessageList = new ArrayList<PatientQuestion>();
	
	private static long mDocID = 1;
	
	// API to get the dl file path
	public static String getDownloadFilePath(int fileType) {
        String state = Environment.getExternalStorageState();
        String filePath = null;
        
        if (state.equals(Environment.MEDIA_MOUNTED)) {
        	filePath = new String(Environment.getExternalStorageDirectory()
                    + File.separator + "medsixplus" + File.separator);
        	switch(fileType) {
        	case DataConstants.ATTACH_TYPE_PIC:
        		filePath += "image" + File.separator;
        		break;
        	case DataConstants.ATTACH_TYPE_WAV:
        		filePath += "audio" + File.separator;
        	}
        }
        return filePath;
    }
	
	// API to get doc id
	public static long getSelfID() {
		return mDocID;
	}
	
	public static int parseLoginResp(JSONObject jso) {
		int ret = -1;

        if(jso!=null){
        	xHelper.log(jso.toString());
        	try{
        		if(jso.getBoolean("success")) {
        			//result array with the returned rows 
        			JSONObject result = jso.getJSONObject("result");
        			mDocID = result.getLong("doc_id");
        			xHelper.log("Login Successfully!! Doc id " + mDocID);
        			ret = 0;
        		}
        		else {
        			xHelper.log("Login Failed!!");
        		}
        	} catch (Exception e) {
        		e.printStackTrace();
        	}
        }
        
        return ret;
	}
	
	public static int parseGetPatientsResp(JSONObject jso) {
		int ret = 0;
		
		mAskingQuesPatientList.clear();
        try{
        	if(jso.getBoolean("success")) {
        		JSONArray patientList = jso.getJSONArray("result");
                if(patientList!=null && patientList.length()>0){
                	//clear the category list first
                	mAskingQuesPatientList.clear();
                	
                    for(int i=0;i<patientList.length();i++){
                    	Patient patient = new Patient();
                    	
                        JSONObject obj = patientList.getJSONObject(i);
                        patient.name = obj.getString("name");
                        patient.address = obj.getString("address");
                        patient.birthdate = obj.getString("birthdate");
                        patient.pat_id = obj.getLong("pat_id");
                        patient.sex = obj.getInt("sex");
                        
                        mAskingQuesPatientList.add(patient);
                    }
                    
                }
                ret = 0;
        	} else {
        		ret = -1;
        	}
            
            
		}catch(Exception e){
			e.printStackTrace();
			ret = -1;
		}
        
        return ret;
	}
	
	public static void addQuestion(PatientQuestion c) {
		mMessageList.add(c);
	}
	
	public static void addDocResponse(String resp) {
		//need first get the question id
		//this time, just use a fake one
		long quesId = 1;
		
		//get the max msg_id of given question_id
		int maxMsgId = 1;
		int index = 0;
		for(PatientQuestion p:mMessageList) {
			if (p.question_id == quesId) {
				if(p.message_id > maxMsgId) {
					index = mMessageList.indexOf(p);
					maxMsgId = p.message_id;
				}
			}
		}
		PatientQuestion newQuest = new PatientQuestion(mMessageList.get(index));
		newQuest.attach_type = 0;
		newQuest.direct = 1;
		newQuest.attach_url = null;
		newQuest.attach_local_url = null;
		newQuest.question_desc = new String(resp);
		newQuest.message_id = maxMsgId + 1;
		
		//add the message to local UI
		mMessageList.add(newQuest);
	}
	
	// Get the question ids of a patient
	public static int parseGetQuestionResp(JSONObject jso) {
		int ret = 0;
		ArrayList<QuestionDigest> tmpList = new ArrayList<QuestionDigest>();

		try{
			if(jso.getBoolean("success")) {
				JSONArray questionIDList = jso.getJSONArray("result");
				if(questionIDList!=null && questionIDList.length()>0){
					for(int i=0;i<questionIDList.length();i++){
						JSONObject obj = questionIDList.getJSONObject(i);
						QuestionDigest quesDigest = new QuestionDigest();
						boolean found = false;
						
						quesDigest.create_date = obj.getString("create_date");
						quesDigest.local_max_seq_id = 0;
						quesDigest.remote_max_seq_id = obj.getLong("max_seq_id");
						quesDigest.question_id = obj.getLong("question_id");
						quesDigest.new_msg = true;
						
						// Update the local array
						for(QuestionDigest p : mQuestionDigestList) {
							if(p.question_id == quesDigest.question_id
								&& p.local_max_seq_id < quesDigest.remote_max_seq_id) {
								int index = mQuestionDigestList.indexOf(p);
								p.new_msg = true;
								p.remote_max_seq_id = quesDigest.remote_max_seq_id;
								mQuestionDigestList.set(index, p);
								found = true;
							}
						}
						
						if(!found)
							mQuestionDigestList.add(quesDigest);
						
					}
				}
			} else {
				ret = -1;
			}
		}catch(Exception e){
			e.printStackTrace();
			ret = -1;
		}

		return ret;
	}
	
	// Get the messages of a question
	public static int parseGetMessagesResp(JSONObject jso) {
		int ret = 0;
		
		mMessageList.clear(); // remove it later!

		//xHelper.log("qes size b4 " + mMessageList.size());
        try{
        	if(jso.getBoolean("success")) {
        		JSONArray questionList = jso.getJSONArray("result");
                if(questionList!=null && questionList.length()>0){
                	//clear the category list first
                	mMessageList.clear();

                    for(int i=0;i<questionList.length();i++){
                    	PatientQuestion question = new PatientQuestion();
                    	
                        JSONObject obj = questionList.getJSONObject(i);
                        //question.seq_id = obj.getLong("seq_id");
                        question.pat_id = obj.getLong("pat_id");
                        question.doc_id = obj.getLong("doc_id");
                        question.question_id = obj.getInt("question_id");
                        question.message_id = obj.getInt("message_id");
                        question.direct = obj.getInt("direct");
                        question.question_desc = obj.getString("question_desc");
                        question.attach_type = obj.getInt("attach_type");
                        question.attach_url = obj.getString("attach_url");
                        question.attach_local_url = null;
                        
                        mMessageList.add(question);
                        
                    }

                }
        	} else {
        		ret = -1;
        	}
		}catch(Exception e){
			e.printStackTrace();
			ret = -1;
		}
        
        //xHelper.log("qes size " + mMessageList.size());
        return ret;
	}
}
