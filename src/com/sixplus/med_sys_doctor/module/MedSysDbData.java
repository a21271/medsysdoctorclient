package com.sixplus.med_sys_doctor.module;

import java.util.ArrayList;
import com.sixplus.med_sys_doctor.util.DataStruct.*;
 
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class MedSysDbData extends SQLiteOpenHelper {
	 
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
    private static final String DATABASE_NAME = "medsys";
 
    // table name
    private static final String TABLE_PATIENTS = "td_patients";
	private static final String TABLE_QUESTIONS = "tf_questions";
 
    // Table Columns names
	private static final String KEY_SEQ_ID = "seq_id";
    private static final String KEY_PAT_ID = "pat_id";
    private static final String KEY_LOGIN_ID = "login_id";
    //private static final String KEY_ROLE_TYPE = "role_type";
    private static final String KEY_NAME = "name";
    private static final String KEY_SEX = "sex";
    private static final String KEY_ADDRESS = "address";
    private static final String KEY_BIRTHDATE = "birthdate";
	
    //private static final String KEY_DOC_ID = "doc_id";
    private static final String KEY_QUESTION_ID = "qeustion_id";
    private static final String KEY_MESSAGE_ID = "message_id";
    private static final String KEY_DIRECT = "direct";
    private static final String KEY_QUESTION_DESC = "question_desc";
    private static final String KEY_ATTACH_TYPE = "attach_type";
    private static final String KEY_ATTACH_URL = "attach_url";
	private static final String KEY_ATTACH_LOCAL_URL = "attach_local_url";
	private static final String KEY_CREATE_DATE = "create_date";
	private static final String KEY_READ_DATE = "read_date";
 
    public MedSysDbData(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
 
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE = "CREATE TABLE " + TABLE_PATIENTS + "("
                + KEY_PAT_ID + " INTEGER PRIMARY KEY," 
        		+ KEY_NAME + " TEXT,"
        		+ KEY_SEX + " INTEGER,"
        		+ KEY_ADDRESS + " TEXT,"
        		+ KEY_BIRTHDATE + " TEXT,"
        		+ KEY_LOGIN_ID + " TEXT," + ")";
				
        db.execSQL(CREATE_TABLE);
		
		CREATE_TABLE = "CREATE TABLE " + TABLE_QUESTIONS + "("
                + KEY_SEQ_ID + " INTEGER PRIMARY KEY," 
        		+ KEY_PAT_ID + " INTEGER,"
				+ KEY_QUESTION_ID + " INTEGER,"
				+ KEY_MESSAGE_ID + " INTEGER,"
				+ KEY_DIRECT + " INTEGER,"
        		+ KEY_QUESTION_DESC + " TEXT,"
				+ KEY_ATTACH_TYPE + " INTEGER,"
        		+ KEY_ATTACH_URL + " TEXT,"
				+ KEY_ATTACH_LOCAL_URL + " TEXT,"
        		+ KEY_CREATE_DATE + " TEXT,"
        		+ KEY_READ_DATE + " TEXT," + ")";
		
		db.execSQL(CREATE_TABLE);
    }
 
    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PATIENTS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTIONS);
 
        // Create tables again
        onCreate(db);
    }
 
    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */
	// Delete API
	public void deleteTableQuestions() {
    	SQLiteDatabase db = this.getWritableDatabase();
    	try {
    		db.execSQL("delete * from " + TABLE_QUESTIONS);
    	} finally {
    		db.close();
    	}
    }
	
	public void deleteTablePatients() {
    	SQLiteDatabase db = this.getWritableDatabase();
    	try {
    		db.execSQL("delete * from " + TABLE_PATIENTS);
    	} finally {
    		db.close();
    	}
    }
	
	// Insert API
	private ContentValues populateQuestion(PatientQuestion p) {
    	
        ContentValues values = new ContentValues();
        values.put(KEY_SEQ_ID, p.seq_id);
        values.put(KEY_PAT_ID, p.pat_id);
        values.put(KEY_QUESTION_ID, p.question_id);
		values.put(KEY_MESSAGE_ID, p.message_id);
		values.put(KEY_DIRECT, p.direct);
		values.put(KEY_QUESTION_DESC, p.question_desc);
		values.put(KEY_ATTACH_TYPE, p.attach_type);
		values.put(KEY_ATTACH_URL, p.attach_url);
		values.put(KEY_ATTACH_LOCAL_URL, p.attach_local_url);
		values.put(KEY_READ_DATE, p.read_date);
 
        return values;

    }
	
	public void insertToTableQuestions(PatientQuestion p) {	
		SQLiteDatabase db = this.getWritableDatabase();
    	
    	try {
    		db.insert(TABLE_QUESTIONS, null, populateQuestion(p));
    	} finally {
    		db.close(); // Closing database connection
    	}
	}
	
	public void insertToTableQuestions(ArrayList<PatientQuestion> a) {	
		SQLiteDatabase db = this.getWritableDatabase();
    	
    	try {
			for(PatientQuestion p:a) {
        		db.insert(TABLE_QUESTIONS, null, populateQuestion(p));
        	}
    	} finally {
    		db.close(); // Closing database connection
    	}
	}

	private ContentValues populatePatients(Patient p) {
    	
        ContentValues values = new ContentValues();
        values.put(KEY_PAT_ID, p.pat_id);
        values.put(KEY_NAME, p.name);
		values.put(KEY_SEX, p.sex);
		values.put(KEY_ADDRESS, p.address);
		values.put(KEY_BIRTHDATE, p.birthdate);
		values.put(KEY_LOGIN_ID, p.login_id);
 
        return values;

    }
	
	public void insertToTablePatients(Patient p) {	
		SQLiteDatabase db = this.getWritableDatabase();
    	
    	try {
    		db.insert(TABLE_PATIENTS, null, populatePatients(p));
    	} finally {
    		db.close(); // Closing database connection
    	}
	}
	
	public void insertToTablePatients(ArrayList<Patient> a) {	
		SQLiteDatabase db = this.getWritableDatabase();
    	
    	try {
			for(Patient p:a) {
        		db.insert(TABLE_PATIENTS, null, populatePatients(p));
        	}
    	} finally {
    		db.close(); // Closing database connection
    	}
	}
 
    // Read API
	// Read of tf_questions table
    public ArrayList<PatientQuestion> readQuestionsFromDb(String cond) {
    	ArrayList<PatientQuestion> quesList = new ArrayList<PatientQuestion>();

        // Select All Query
        String selectQuery = "SELECT  "
        		+ KEY_SEQ_ID + " , "
        		+ KEY_PAT_ID + " , "
        		+ KEY_QUESTION_ID + " , "
        		+ KEY_MESSAGE_ID + " , "
        		+ KEY_DIRECT + " , "
        		+ KEY_QUESTION_DESC + " , "
        		+ KEY_ATTACH_TYPE + " , "
        		+ KEY_ATTACH_URL + " , "
        		+ KEY_ATTACH_LOCAL_URL + " , "
        		+ KEY_CREATE_DATE + " , "
        		+ KEY_READ_DATE
        		+ " FROM " + TABLE_QUESTIONS;
 
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
		
        // looping through all rows and adding to list
        // !!IMPORTANT: pay attention to the column sequence
        if (cursor.moveToFirst()) {
        	try {
        		do {
        			PatientQuestion p = new PatientQuestion();
                	p.seq_id = Long.parseLong(cursor.getString(0));
                	p.pat_id = Long.parseLong(cursor.getString(1));
                	p.question_id = Integer.parseInt(cursor.getString(2));
                	p.message_id = Integer.parseInt(cursor.getString(3));
                	p.direct = Integer.parseInt(cursor.getString(4));
                	p.question_desc = cursor.getString(5);
                	p.attach_type = Integer.parseInt(cursor.getString(6));
                	p.attach_url = cursor.getString(7);
                	p.attach_local_url = cursor.getString(8);
                	p.create_date = cursor.getString(9);
                	p.read_date = cursor.getString(10);
                	
                    // Adding contact to list
                    quesList.add(p);
                } while (cursor.moveToNext());
        	} finally {
        		cursor.close();
        	}
            
        }
 
        db.close();

        // return contact list
        return quesList;
    }
    
    // Read the question digest list
    public void getQuestionDigest(long patID, long docID, ArrayList<QuestionDigest> list)
    {
        String selectQuery = "select question_id, create_date, seq_id from tf_questions "
        		+ " where seq_id = "
        		+ "(select max(seq_id) from tf_questions "
        		+ " where pat_id = " + patID + " and doc_id = " + docID + " )";
 
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        
        // looping through all rows and adding to list
        // !!IMPORTANT: pay attention to the column sequence
        if (cursor.moveToFirst()) {
        	try {
        		do {
        			QuestionDigest p = new QuestionDigest();
                	p.question_id = Integer.parseInt(cursor.getString(0));
                	p.create_date = cursor.getString(1);
                	p.local_max_seq_id = Long.parseLong(cursor.getString(2));
                	p.remote_max_seq_id = 0;
                    // Adding contact to list
                    list.add(p);
                } while (cursor.moveToNext());
        	} finally {
        		cursor.close();
        	}
            
        }
        
        db.close();
    }
    // Updating single contact
    // To do
 
    // Deleting single contact
    // To do
 
    // Getting contacts Count
	// To do
 
}