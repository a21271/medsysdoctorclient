package com.sixplus.med_sys_doctor;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import android.os.Message;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import com.sixplus.med_sys_doctor.module.MedSysData;
import com.sixplus.med_sys_doctor.module.MedSysDbData;
import com.sixplus.med_sys_doctor.util.DataStruct.PatientQuestion;

import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;

import us.xdroid.util.xHelper;
import us.xdroid.util.HttpConnectionManager;
import us.xdroid.util.HttpConnectionManager.*;


public class MedController extends us.xdroid.util.ControllerBase{

    public static final int LOGIN = 1000;
    public static final int REQ_ASK_QUESTION_PATIENTS_LIST = 1001;
    public static final int REQ_PATIENT_QUESTIONS_LIST = 1002;
    public static final int REQ_MSGS_OF_QUESTION = 1003;
    public static final int REQ_PATIENT_INQUIRE_PIC = 1004;
    public static final int DL_ATTACHMENT = 1005;
    public static final int UPLOAD_DOC_RESPONSE = 1006;
    
    /* add new action here */

    static MedController mController = null;
    static MedSysDbData mDatabase;

    public MedController(Context context){
        //HttpConnectionManager.SERVER_URL = "http://192.168.1.65:8888/modules/Mobile/api.php";
        HttpConnectionManager.SERVER_URL = "http://192.168.1.134:8888/modules/Mobile/api.php";
    	//HttpConnectionManager.SERVER_URL = "http://54.214.27.29/vtigercrm/modules/Mobile/api.php";
        init(context);

        mController = this;
        mDatabase = new MedSysDbData(context);
    }
    
    @Override
    public void handleMessage(Message msg){
        switch(msg.what){
            case LOGIN:
                handleLoginReq(msg);
                break;
            case REQ_ASK_QUESTION_PATIENTS_LIST:
            	handleAskQuestionPatients(msg);
            	break;
            case REQ_PATIENT_QUESTIONS_LIST:
            	handlePatientQuestionsList(msg);
            	break;
            case REQ_MSGS_OF_QUESTION:
            	handleRequireMsgsOfQuestion(msg);
            	break;
            /*case REQ_PATIENT_INQUIRE_PIC:
            	handleReuirePatientInquirePic(msg);
            	break;*/
            case DL_ATTACHMENT:
            	handleDownloadAttachment(msg);
            	break;
            case UPLOAD_DOC_RESPONSE:
            	handleUploadDocResponse(msg);
            	break;
            default:
                handleGenericReq(msg);
                break;
        }
	}
    

	static MyJsonResponse doPostJson(JSONObject obj){
        return HttpConnectionManager.doPostJson(obj);
    }
    
    
    void handleGenericReq(Message msg){
        MyJsonResponse resp = doPostJson((JSONObject)msg.obj);
        
        JSONObject jso = resp.obj;
        if(jso!=null){
            sendMessageToUI(msg.what,0,0,null);
            return;
        }
        sendMessageToUI(msg.what,-1,0,null);
        
    }
    
    // Public API to login
    public static void login(String username,String password){
        JSONObject obj = new JSONObject();
        try {
			obj.put("_operation", new String("doc"));
			obj.put("sub", new String("login"));
			obj.put("login_id", username);
			obj.put("login_password", password);
			
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            return;
        }
        mController.sendRequest(LOGIN,obj);
    }
    
    // Public API to get the patients list who asked questions
    public static void getAskQuestionPatientList(long docID, long seqID){
    	JSONObject obj = new JSONObject();
        try {
			obj.put("_operation", new String("que"));
			obj.put("sub", new String("queryPat"));
			obj.put("doc_id", docID);
			//obj.put("seq_id", docID);
			
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            return;
        }
        mController.sendRequest(REQ_ASK_QUESTION_PATIENTS_LIST,obj);
    }
    
    // Public API to get the questions of a patient
    public static void getNewQuestionsOfPatient(long patID, long docID){
    	JSONObject obj = new JSONObject();
        try {
			obj.put("_operation", new String("que"));
			obj.put("sub", new String("queryList"));
			obj.put("doc_id", docID);
			obj.put("pat_id", patID);
			
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            return;
        }
        mController.sendRequest(REQ_PATIENT_QUESTIONS_LIST,obj);
    }
    
    // Public API to get the old questions of patient (store in local db)
    public static void getOldQuestionsOfPatient(long patID, long docID){
    	// Clear out first
    	MedSysData.mQuestionDigestList.clear();
    	mDatabase.getQuestionDigest(patID, docID, MedSysData.mQuestionDigestList);
    }
    
    // Public API to get the messages of one question
    public static void getMessagesOfQuestion(long questionID){
    	JSONObject obj = new JSONObject();
        try {
			obj.put("_operation", new String("que"));
			obj.put("sub", new String("query"));
			obj.put("question_id", questionID);
			
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            return;
        }
        mController.sendRequest(REQ_MSGS_OF_QUESTION,obj);
    }

    public static void downloadAttachment(String url, long patID, int questID, int msgID, int attachType ){
        JSONObject obj = new JSONObject();
        try {
			obj.put("_internal_operation", new String("download_attachment"));
			obj.put("url", url);
			obj.put("question_id", questID);
			obj.put("pat_id", patID);
			obj.put("message_id", msgID);
			obj.put("attach_type", attachType);
			
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            return;
        }
        mController.sendRequest(DL_ATTACHMENT,obj);
    }
    
    // Public API to upload the doctor response to the question
    public static void uploadDocResponse(PatientQuestion quest){
        JSONObject obj = new JSONObject();
        try {
			obj.put("_operation", new String("que"));
			obj.put("sub", new String("addMessage"));
			obj.put("pat_id", quest.pat_id);
			obj.put("doc_id", quest.doc_id);
			obj.put("question_id", quest.question_id);
			//obj.put("message_id", quest.message_id);
			obj.put("direct", quest.direct);
			obj.put("message", quest.question_desc);
			obj.put("attach_type", quest.attach_type);
			obj.put("attach_url", "null");
			
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            return;
        }
        mController.sendRequest(UPLOAD_DOC_RESPONSE,obj);
    }
    
    // Do real work of logging
    void handleLoginReq(Message msg){
    	MyJsonResponse resp = doPostJson((JSONObject)msg.obj);
    	
    	int status = MedSysData.parseLoginResp((JSONObject)resp.obj);
		sendMessageToUI(LOGIN, status, 0, null);
    }
    
    // Do the real work of downloading picture
    private void handleDownloadAttachment(Message msg) {
    	JSONObject jso = (JSONObject)msg.obj;
		
    	try{
    		xHelper.log("trying to dl attachment");
    		xHelper.log(jso.toString());
    		long patId = jso.getLong("pat_id");
    		int msgId = jso.getInt("message_id");
    		int questId = jso.getInt("question_id");
    		int attachType = jso.getInt("attach_type");
    		URL url = new URL(jso.getString("url"));
    		URLConnection connection = url.openConnection();
    		connection.connect();
    		xHelper.log("try connect to " + url.toString());    		
    		int fileLength = connection.getContentLength();
    		
    		xHelper.log("file length" + fileLength);
    		String fileExtenstion = MimeTypeMap.getFileExtensionFromUrl(url.toString());
    		String fileName = URLUtil.guessFileName(url.toString(), null, fileExtenstion);
    		xHelper.log("file name " + fileName);
    		
    		//get the local file path
    		String localFilePath = MedSysData.getDownloadFilePath(attachType);
    		File dir = new File(localFilePath);
    		if (!dir.exists()) {
    			dir.mkdir();
    		}
    		if(!dir.exists()) {
    			xHelper.log("create file failed!");
    			return;
    		}

    		InputStream input = new BufferedInputStream(url.openStream());
    		//OutputStream output = this.getContext().openFileOutput(fileName, Context.MODE_PRIVATE);
    		FileOutputStream output = new FileOutputStream(localFilePath + fileName);
    		
    		byte data[] = new byte[1024];
    		long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                //publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();
            
            // Change the local url of the questions
            int index = 0;
            String attachLocalUrl = localFilePath + fileName;;
            for(PatientQuestion p : MedSysData.mMessageList) {

            	if(p.pat_id == patId && p.message_id == msgId && p.question_id == questId) {
            		p.attach_local_url = attachLocalUrl;
            		MedSysData.mMessageList.set(index, p);
            		xHelper.log("set the local url " + p.attach_local_url + " for index " + index);
            	}
            	
            	index++;
            }
            
            sendMessageToUI(DL_ATTACHMENT,0,0,attachLocalUrl);
    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		sendMessageToUI(DL_ATTACHMENT,-1,0,null);
    	}
	}

    // Do the real work of getting questions list of a patient
    private void handlePatientQuestionsList(Message msg) {
    	MyJsonResponse resp = doPostJson((JSONObject)msg.obj);
    	JSONObject jso = resp.obj;

		int status = MedSysData.parseGetQuestionResp((JSONObject)jso);
		sendMessageToUI(REQ_PATIENT_QUESTIONS_LIST, status, 0, null);
	}
    
    // Do the real work of getting patients list who asked a question
    private void handleAskQuestionPatients(Message msg){
    	MyJsonResponse resp = doPostJson((JSONObject)msg.obj);
    	JSONObject jso = resp.obj;
    	
    	int status = MedSysData.parseGetPatientsResp((JSONObject)jso);
    	sendMessageToUI(REQ_ASK_QUESTION_PATIENTS_LIST, status, 0, null);
    }
    
    // Do the real work of getting message list of a question
    private void handleRequireMsgsOfQuestion(Message msg){
    	MyJsonResponse resp = doPostJson((JSONObject)msg.obj);
    	JSONObject jso = resp.obj;
    	
    	int status = MedSysData.parseGetMessagesResp((JSONObject)jso);
    	sendMessageToUI(REQ_MSGS_OF_QUESTION, status, 0, null);
    }
    
    // Do the real work of uploading the doctor response
    private void handleUploadDocResponse(Message msg){
    	MyJsonResponse resp = doPostJson((JSONObject)msg.obj);
    	JSONObject jso = resp.obj;
    	
    	xHelper.log(jso.toString());
    	try{
    		if(jso.getBoolean("success")) {
    			sendMessageToUI(UPLOAD_DOC_RESPONSE,0,0,0);
    		}
    		else {
    			sendMessageToUI(UPLOAD_DOC_RESPONSE,-1,0,0);
    		}
    	}  catch (Exception e) {
    		e.printStackTrace();
    		sendMessageToUI(UPLOAD_DOC_RESPONSE,-1,0,null);
    	}
    }
    
}