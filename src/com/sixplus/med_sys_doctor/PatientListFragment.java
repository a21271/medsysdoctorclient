package com.sixplus.med_sys_doctor;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.os.Handler;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;

import com.haarman.listviewanimations.swinginadapters.prepared.SwingBottomInAnimationAdapter;
import com.sixplus.med_sys_doctor.module.MedSysData;
import com.sixplus.med_sys_doctor.util.DataConstants;
import com.sixplus.med_sys_doctor.util.DataStruct.Patient;

import us.xdroid.util.xHelper;
import us.xdroid.util.xUtil;


public class PatientListFragment extends Fragment
  implements AdapterView.OnItemClickListener
{
  private ListView listView;
  PatientListAdapter patientListAdapter;
  String mPatientType = null;

  public void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);

    xHelper.log("PatientListFragment");

    patientListAdapter.notifyDataSetChanged();

  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    
    listView = new ListView(getActivity());
    listView.setOnItemClickListener(this);
    
    Intent intent = getActivity().getIntent();
    mPatientType = intent.getStringExtra(DataConstants.TAG_PATIENT_TYPE);
    
    if (mPatientType.equals(DataConstants.PATIENT_TYPE_ASKING_QUESTION)) {
    	patientListAdapter = new PatientListAdapter(getActivity(), MedSysData.mAskingQuesPatientList);
    } else if (mPatientType.equals(DataConstants.PATIENT_TYPE_QUESTION_READ)) {
    	patientListAdapter = new PatientListAdapter(getActivity(), MedSysData.mReadQuesPatientList);
    } else if (mPatientType.equals(DataConstants.PATIENT_TYPE_QUESTION_OLD)) {
    	patientListAdapter = new PatientListAdapter(getActivity(), MedSysData.mOldQuesPatientList);
    }

    SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(patientListAdapter);
    swingBottomInAnimationAdapter.setAbsListView(listView);
    listView.setAdapter(swingBottomInAnimationAdapter);

    listView.setCacheColorHint(0);
    return this.listView;
  }

  void doRetrieve(){
	  
  }
  

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
	  
	  Intent intent = new Intent(Intent.ACTION_VIEW);
	  intent.setClass(this.getActivity(),  PatientQuestionListActivity.class);
	  //intent.putExtra("patient_index", paramInt-1);
	  intent.putExtra("pat_id", (Long)paramView.getTag());
	  intent.putExtra(DataConstants.TAG_PATIENT_TYPE, mPatientType);
	  startActivity(intent);
  }

  public void onPause()
  {
    super.onPause();
  }

  public void onResume()
  {
    super.onResume();
  }

  class PatientListAdapter extends com.haarman.listviewanimations.ArrayAdapter<Patient>{
      Context mContext;
      ArrayList<Patient> mItems;
      
      public PatientListAdapter(Context c, ArrayList<Patient> items) {
  		super(items);
  		mContext = c;
  		mItems = items;
  	  }
      
      @Override
      public int getCount() {
    	  //xHelper.log("MyListAdapter::getCount");
    	  return mItems.size();
      }
      
      @Override
      public long getItemId(int position) {
          // TODO Auto-generated method stub
          return position;
      }
      
      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
          LayoutInflater inflater = LayoutInflater.from(mContext);
          View localView = inflater.inflate(R.layout.patient_item, null);
          Patient pat = getItem(position);
          int age = 0;
          
          SimpleDateFormat  format = new SimpleDateFormat("yyyy-mm-dd");
          try {
			Date date = format.parse(pat.birthdate);
			age = xUtil.getAge(date.getYear() + 1900, date.getMonth(), date.getDay());
          } catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
          }
          
         if(age >= 150 || age <= 0) {
        	 age = -1;
         }
         
         ((TextView)localView.findViewById(R.id.patient_id)).setText(Long.toString(pat.pat_id));
         ((TextView)localView.findViewById(R.id.patient_name)).setText(pat.name);
         ((TextView)localView.findViewById(R.id.patient_sex)).setText(pat.sex == 0 ? "性别：男" : "性别：女");
         ((TextView)localView.findViewById(R.id.patient_age)).setText("年龄：" + (age == -1 ? "未知" : Integer.toString(age)));
         
         // set the patient_id for later use
         localView.setTag(pat.pat_id);
          
          //xHelper.log("getView-------");
          return localView;
      }

	@Override
	public Patient getItem(int arg0) {
		// TODO Auto-generated method stub
		return mItems.get(arg0);
	}

  }
  
}