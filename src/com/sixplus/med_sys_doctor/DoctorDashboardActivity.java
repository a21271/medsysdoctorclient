package com.sixplus.med_sys_doctor;

import com.sixplus.med_sys_doctor.module.MedSysData;
import com.sixplus.med_sys_doctor.util.DataConstants;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DoctorDashboardActivity extends Activity {
	private static final String TAG = "DoctorDashboardActivity";
	private ProgressDialog mProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doc_main);
  	  	
        //title
        ((TextView)findViewById(R.id.navigation_title)).setText(R.string.doctor_dashboard);
        
  	  	//Set the UI handler
  	  	MedController.setUIHandler(mUiHandler);
  	  
        // Edit Text
        LinearLayout remoteInquire = (LinearLayout) findViewById(R.id.remote_inquire);
        LinearLayout appointment = (LinearLayout) findViewById(R.id.appointment);
        LinearLayout finishedInquire = (LinearLayout) findViewById(R.id.finished_inquire);
        TextView quitButton = (TextView) findViewById(R.id.quit);

        remoteInquire.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            	if(MedSysData.mAskingQuesPatientList.size() <= 0)
					return;
            	
            	Intent intent = new Intent(Intent.ACTION_VIEW);
            	intent.putExtra(DataConstants.TAG_PATIENT_TYPE, DataConstants.PATIENT_TYPE_ASKING_QUESTION);
            	intent.setClass(DoctorDashboardActivity.this, PatientListActivity.class);
            	startActivity(intent);
            }
        });
        
        quitButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
        
        // Calendar button
        Button calendar = (Button)findViewById(R.id.calendar);
        calendar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setClass(DoctorDashboardActivity.this, DoctorCalendarActivity.class);
            	startActivity(intent);
			}
		});
        
        // Get the patient list who asked the questions
        MedController.getAskQuestionPatientList(MedSysData.getSelfID(), 1234); //change the seqid later!!
        
        // Add Get the patient list whose questions has been read but no response
        // To do
        
        // Add get the patient list whose questions is old
        // To do
        
        showProgressDialog();
        
    }

    //implement the UI handler
   final Handler mUiHandler = new Handler(new Handler.Callback() {
		
		@Override
		public boolean handleMessage(Message msg) {
			// TODO Auto-generated method stub
			switch (msg.what) {
			case  MedController.REQ_ASK_QUESTION_PATIENTS_LIST:
                //Medcontroller will sent this message back after it handled the request
				if(msg.arg1 == 0) {
					Toast.makeText(DoctorDashboardActivity.this, "get patient list success", Toast.LENGTH_LONG).show();
					final TextView remoteDiag = ((TextView)findViewById(R.id.diagonse_require));
					String oldStr = remoteDiag.getText().toString();
					remoteDiag.setText(oldStr + "(" + MedSysData.mAskingQuesPatientList.size() + ")");
				}
				else {
					Toast.makeText(DoctorDashboardActivity.this, "get patient list failed!", Toast.LENGTH_LONG).show();
				}
				closeProgressDialog();
              break;
	        }
			return false;
		}
	});
	
	private void showProgressDialog() {
    	mProgressDialog = new ProgressDialog(DoctorDashboardActivity.this);
        mProgressDialog.setMessage("Process...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();
    }
	
	private void closeProgressDialog() {
    	mProgressDialog.dismiss();
    }

}
